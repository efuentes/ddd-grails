package com.softtek.erp.domain.taxonomy

class Vocabulary {

  String code
  String name
  String description
  Boolean restricted = false

  def Vocabulary(String code) {
    this.code = code
  }

  Vocabulary withName(String name) {
    this.name = name

    return this
  }

  Vocabulary withDescription(String description) {
    this.description = description

    return this
  }

}