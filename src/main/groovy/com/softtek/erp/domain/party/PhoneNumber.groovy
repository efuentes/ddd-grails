package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class PhoneNumber {

  String number
  String countryCode
  String areaCode
  Term type

  def PhoneNumber(String number, Term type) {
    this.number = number
    this.type = type
  }

  PhoneNumber withCountryCode(String code) {
    this.countryCode = code

    return this
  }

  PhoneNumber withAreaCode(String code) {
    this.areaCode = code

    return this
  }
}