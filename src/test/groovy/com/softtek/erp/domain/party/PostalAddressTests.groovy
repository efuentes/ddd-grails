package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class PostalAddressTests {

  @Test
  void canCreatePostalAddressWithAddressesGeographicBoundary() {

    // Arrange
    def geographicBoundaryVocabulary = new Vocabulary("GEOGRAPHIC_BOUNDARY")
    def countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary).withName("Country")
    def geoCountryMex = new GeographicBoundary("MX", countryTerm).withName("México").withAbbreviation("Mex")
    def stateTerm = new Term("STATE", geographicBoundaryVocabulary).withName("State")
    def geoStateMexDF = new GeographicBoundary("MX-DIF", stateTerm).withParent(geoCountryMex)
    geoStateMexDF.withName("Distrito Federal").withAbbreviation("D.F.")

    // Act
    PostalAddress sut = new PostalAddress("Address 1", geoStateMexDF).withAddress2("Address 2")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.address1, is("Address 1")
    assertThat sut.address2, is("Address 2")
    assertThat sut.geographicBoundary.code, is("MX-DIF")
  }
}