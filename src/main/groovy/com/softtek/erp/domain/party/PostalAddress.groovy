package com.softtek.erp.domain.party

class PostalAddress {

  String address1
  String address2
  GeographicBoundary geographicBoundary

  def PostalAddress(String address1, GeographicBoundary geographicBoundary) {
    this.address1 = address1
    this.geographicBoundary = geographicBoundary
  }

  PostalAddress withAddress2(String address2) {
    this.address2 = address2

    return this
  }
}