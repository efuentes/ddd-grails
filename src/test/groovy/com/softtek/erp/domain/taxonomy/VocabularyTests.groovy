package com.softtek.erp.domain.taxonomy

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

import org.junit.*

class VocabularyTests {

  @Test
  void canCreateTaxomonywithCodeName() {

    Vocabulary sut = new Vocabulary("ORGANIZATION_TYPE").withName("Organization Type")

    assertThat sut, notNullValue()
    assertThat sut.code, equalTo("ORGANIZATION_TYPE")
    assertThat sut.name, equalTo("Organization Type")
    assertThat sut.restricted, equalTo(false)
  }

  @Test
  void canCreateVocabularyWithCodeNameDescription() {

    Vocabulary sut = new Vocabulary("ORGANIZATION_TYPE").withName("Organization Type").withDescription("Types of Organization.")

    assertThat sut, notNullValue()
    assertThat sut.code, equalTo("ORGANIZATION_TYPE")
    assertThat sut.name, equalTo("Organization Type")
    assertThat sut.description, equalTo("Types of Organization.")
    assertThat sut.restricted, equalTo(false)
  }
}