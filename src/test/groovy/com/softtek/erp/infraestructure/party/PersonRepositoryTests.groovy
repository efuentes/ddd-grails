package com.softtek.erp.infraestructure.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import static java.util.UUID.randomUUID
import groovy.sql.Sql

import com.softtek.erp.domain.party.Person

class PersonRepositoryTests {

  static def sql

  @BeforeClass
  static void setup() {
    def db = [url:'jdbc:mysql://127.0.0.1:3306/erp-dev', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    sql = Sql.newInstance(db.url, db.user, db.password, db.driver)
  }

  @AfterClass
  static void teardown() {
    sql.close()
  }

  @Before
  void before() {
    sql.executeUpdate("DELETE FROM person")
  }

  @Test
  void canFindPersonByName() {

    // Arrange
    def personTable = sql.dataSet("person")
    def uuid = randomUUID() as String
    personTable.add( uuid: uuid,
                     first_name: "John",
                     last_name: "Smith"
                    )

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    Person sut = repository.findOneByFirstNameAndLastName("John", "Smith")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, is(uuid)
    assertThat sut.name(), is("John Smith")
  }

  @Test
  void canFindPersonByUUID() {

    // Arrange
    def personTable = sql.dataSet("person")
    def uuid = randomUUID() as String
    personTable.add( uuid: uuid,
                     first_name: "John",
                     last_name: "Smith"
                    )

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    Person sut = repository.findByUUID(uuid)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, is(uuid)
    assertThat sut.name(), is("John Smith")
  }

  @Test
  void canNotFindInexistentPersonByName() {

    // Arrange
    def personTable = sql.dataSet("person")
    def uuid = randomUUID() as String
    personTable.add( uuid: uuid,
                     first_name: "John",
                     last_name: "Smith"
                    )

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    Person sut = repository.findOneByFirstNameAndLastName("Mary", "Smith")

    // Assert
    assertThat sut, nullValue()
  }

  @Test
  void canSavePerson() {

    // Arrange
    Person person = new Person("John", "Smith")

    // Act
    PersonRepositorySqlImpl sut = new PersonRepositorySqlImpl(sql)
    sut.add(person)

    Person savedPerson = sut.findOneByFirstNameAndLastName("John", "Smith")

    // Assert
    assertThat sut, notNullValue()
    assertThat savedPerson.uuid, notNullValue()
    assertThat savedPerson.name(), is("John Smith")
  }
}