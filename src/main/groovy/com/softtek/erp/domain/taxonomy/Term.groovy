package com.softtek.erp.domain.taxonomy

class Term {

  String code
  String name
  Vocabulary vocabulary
  Term parent
  Integer weight
  Boolean restricted = false

  def Term(String code, Vocabulary vocabulary) {
    this.code = code
    this.vocabulary = vocabulary
  }

  def Term(String code, Term parent) {
    this.code = code
    this.parent = parent
    this.vocabulary = parent.vocabulary
  }

  Term withName(String name) {
    this.name = name

    return this
  }

  Term withWeight(Integer weight) {
    this.weight = weight

    return this
  }
}
